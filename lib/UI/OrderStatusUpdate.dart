import 'package:chip_list/chip_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rmt_walnut/UI/StoreScreen.dart';
import 'package:rmt_walnut/main.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

import '../Models/Response/GetOrderRespsonse.dart';
import '../Models/Response/GetOrderbyIdResponse.dart';
import 'DesingScreen.dart';
import 'DispatchScreen.dart';
import 'ProductionScreen.dart';

class OrderStatusUpdate extends StatefulWidget {

  String order_id;
  String projectName;


   OrderStatusUpdate({Key? key,required this.order_id,required this.projectName}) : super(key: key);

  @override
  State<OrderStatusUpdate> createState() => _OrderStatusUpdateState();
}

class _OrderStatusUpdateState extends State<OrderStatusUpdate> {

  final List<String> _dogeNames = [
    'Beagle',
    'Labrador',
    'Retriever',
  ];
  Future<OrderResponseById>?  getf ;
  int _currentIndex = 0;
  List<String> text = ["Design","Store","Production","Dispatch"];
  TextStyle greenStyle = const TextStyle(color: Colors.green,fontWeight: FontWeight.normal,fontStyle: FontStyle.normal);
  TextStyle redText = const TextStyle(color: Colors.red,fontWeight: FontWeight.normal,fontStyle: FontStyle.normal);
  List<Widget> textList =[];
  late Size size ;


  @override
  Widget build(BuildContext context) {

     size = MediaQuery.of(context).size;
    return Scaffold(appBar: AppBar(backgroundColor: Colors.transparent,elevation: 0,leading: IconButton(icon: Icon(Icons.arrow_back,color: Colors.blue,),onPressed: (){
      
      
      Navigator.pop(context);
    }),centerTitle: true,title:Text(widget.projectName.toString(),style: TextStyle(fontStyle: FontStyle.italic,color: Colors.blue),) ),
      body: FutureBuilder(future:getf, builder: (BuildContext context, AsyncSnapshot<OrderResponseById> snapshot) {

        if(snapshot.connectionState == ConnectionState.done){

        if(snapshot.hasData){

        //  logger.d(snapshot.data!.toJson().toString());
        }else{

          logger.e("No Data");
        }

          print(_currentIndex);
          return  SingleChildScrollView(
            child: Column(children: [


              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(height: 50,width: size.width-20,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.grey.shade200,),

                child: ListView.builder(shrinkWrap: true,scrollDirection: Axis.horizontal,itemCount: textList.length,itemBuilder: (context,index){
                  return GestureDetector(
                    onTap: (){

                      setState((){
                        _currentIndex = index;

                        setHeader(text.elementAt(index));
                        print(index.toString() + "  "+index.toString());

                      });

                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(height: 50,width: size.width*0.2,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.grey.shade200,),child: Center(child: textList.elementAt(index),)),),
                  );


                }),

                ),
              ),

             IndexedStack(index: _currentIndex,children: [


               DesignScreen(desing: snapshot.data!.desing,order_id:  snapshot.data!.orderDetails.toString(),),
               StoreScreen(store:  snapshot.data!.store,order_id:  snapshot.data!.orderDetails.toString(),),
               ProductionScreen(production:snapshot.data!.production ,order_id:  snapshot.data!.orderDetails.toString(),),
               DisptchScreen(order_id:  snapshot.data!.orderDetails.toString())


             ],)



            ],),
          );


        }else  if(snapshot.connectionState == ConnectionState.waiting){

          return const Center(child: CircularProgressIndicator(),);


        }
        else {

          return const Center(child: CircularProgressIndicator(),);


        }



      },),




    );
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();



    getf =  ApiProvider(context).getOrdersbyId(widget.order_id).then((value) {


      print("Reloaded");

      setHeader( value.orderStatus.toString());


setDesign(value);


      return value;

    });




  }

  setHeader(String value){

    textList =[];

    if(value =="Design"){


      textList.add(Container(width: size.width*0.2,height: 90,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.white),child: Center(child: Text("Design",style: redText,))),);
      textList.add(Text("Store",style: redText,),);
      textList.add(Text("Production",style: redText,),);
      textList.add(Text("Dispatch",style: redText,),);


      _currentIndex = 0;


    }
    else if(value =="Store"){
      textList.add(Text("Design",style: greenStyle,),);
      textList.add(Container(width: size.width*0.2,height: 90,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.white),child: Center(child: Text("Store",style: redText,))),);
      textList.add(Text("Production",style: redText,),);
      textList.add(Text("Dispatch",style: redText,),);
      _currentIndex = 1;

    }
    else if(value =="Production") {
      textList.add(Text("Design", style: greenStyle,),);
      textList.add(Text("Store", style: greenStyle,),);
      textList.add(Container(width: size.width * 0.2+50,
          height: 90,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white),
          child: Center(
              child: Text("Production", style: redText,))),);
      textList.add(Text("Dispatch", style: redText,),);
      _currentIndex = 2;
    }
    else if(value =="Dispatch"){

      textList.add(Text("Design",style: greenStyle,),);
      textList.add(Text("Store",style: greenStyle,),);
      textList.add(Text("Production",style: greenStyle,),);
      textList.add(Container(width: size.width*0.2,height: 90,decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.white),child: Center(child: Text("Dispatch",style: redText,))),);
      _currentIndex = 3;
    }

    else if(value =="Finished"){

      textList.add(Text("Design",style: greenStyle,),);
      textList.add(Text("Store",style: greenStyle,),);
      textList.add(Text("Production",style: greenStyle,),);
      textList.add(Text("Dispatch", style: greenStyle,),);

      _currentIndex = 0;
    }


  }


  setDesign(OrderResponseById value){




    if(value.desing!=null){




    }

    if(value.store!=null){



    }


    if(value.production !=null){


    }


    if(value.dispatch!=null){


    }


    if(value =="Design") {





    }






  }


}
