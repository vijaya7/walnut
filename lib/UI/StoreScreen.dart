import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Models/Request/StoreUpdateRequest.dart';
import 'package:rmt_walnut/Models/Response/GetOrderbyIdResponse.dart';
import 'package:rmt_walnut/Utils/MyConst.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/main.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

import '../Provider/UserProvider.dart';
class StoreScreen extends StatefulWidget {

  String order_id;
  Store? store;
   StoreScreen({Key? key,this.store,required this.order_id}) : super(key: key);

  @override
  State<StoreScreen> createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {

  DateTime? BoarddDeliverDate;
  DateTime? InksDeliverDate;
  DateTime? VarnishDeliverDate;
  DateTime? LaminationDeliverDate;
  bool board = true;
  bool inks = true;
  bool varnish = true;
  bool lamination = true;

  @override
  Widget build(BuildContext context) {


    List? role = context.read<UserProvider>().userJwtResponse.permisions;

    print(    role!.elementAt(0).toString());
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(children: [




          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Board"),
              CupertinoSwitch(value: board, onChanged: role.elementAt(0).toString()=="Store"?(value){

                setState((){});
                board = value;

              }:null),
            ],
          ),

          const Divider(color: Colors.grey),


          !board ?    Padding(
            padding: const EdgeInsets.all(8.0),


            child:     Container(


              height: 80,
              //   margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.grey.shade300)
              ),
              child:  Column(
                children: [

                  const Text("Please Select Boards Ordered Date"),
                  Row(

                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: BoarddDeliverDate==null? Text("Borad Deliver  Date",style: TextStyle(color: Colors.grey.shade400),):Text(BoarddDeliverDate!.day.toString()+"/"+BoarddDeliverDate!.month.toString()+"/"+BoarddDeliverDate!.year.toString()),
                        ),
                        GestureDetector(
                          onTap: () async {
                            BoarddDeliverDate = await   _selectDate( context);

                            setState(() {

                            });
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(Icons.date_range,color: Colors.blue,),
                          ),
                        )


                      ]),
                ],
              ),
            ),
          ):const SizedBox.shrink(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Inks"),
              CupertinoSwitch(value: inks, onChanged: role.elementAt(0).toString()=="Store"? (value){

                setState((){
                  inks =value;

                });


              }:null),
            ],
          ),
          Divider(color: Colors.grey.shade400,thickness: 1.2,),
          !inks ?    Padding(
            padding: const EdgeInsets.all(8.0),


            child:     Container(


              height: 80,
              //   margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.grey.shade300)
              ),
              child:  Column(
                children: [
                  const Text("Please Select Inks Ordered Date"),
                  Row(

                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: InksDeliverDate==null? Text("Inks Delivery  Date",style: TextStyle(color: Colors.grey.shade400),):Text(InksDeliverDate!.day.toString()+"/"+InksDeliverDate!.month.toString()+"/"+InksDeliverDate!.year.toString()),
                        ),
                        GestureDetector(
                          onTap: () async {
                            InksDeliverDate = await   _selectDate( context);
                            setState(() {

                            });
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(Icons.date_range,color: Colors.blue,),
                          ),
                        )


                      ]),
                ],
              ),
            ),
          ):const SizedBox.shrink(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Varnish"),
              CupertinoSwitch(value: varnish, onChanged:  role.elementAt(0).toString()=="Store"?(value){

                setState((){
                  varnish =value;

                });
              }:null),
            ],
          ),
          Divider(color: Colors.grey.shade400,thickness: 1.5,),
          !varnish ?    Padding(
            padding: const EdgeInsets.all(8.0),


            child:     Container(


              height: 80,
              //   margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.grey.shade300)
              ),
              child:  Column(
                children: [

                  const Text("Please Select Varnish Ordered Date"),
                  Row(

                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: VarnishDeliverDate==null? Text("Varnush Delivey Date",style: TextStyle(color: Colors.grey.shade400),):Text(VarnishDeliverDate!.day.toString()+"/"+VarnishDeliverDate!.month.toString()+"/"+VarnishDeliverDate!.year.toString()),
                        ),
                        GestureDetector(
                          onTap: () async {
                            VarnishDeliverDate = await   _selectDate( context);
                            setState(() {

                            });
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(Icons.date_range,color: Colors.blue,),
                          ),
                        )


                      ]),
                ],
              ),
            ),
          ):const SizedBox.shrink(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text("Lamination"),
              CupertinoSwitch(value: lamination, onChanged: role.elementAt(0).toString()=="Store"? (value){

                setState((){
                  lamination =value;

                });
              }:null),
            ],
          ),
          const Divider(color: Colors.grey),
          !lamination ?    Padding(
            padding: const EdgeInsets.all(8.0),


            child:     Container(


              height: 80,
              //   margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(3.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: Colors.grey.shade300)
              ),
              child:  Column(
                children: [

                  const Text("Please Select Lamination Ordered Date"),
                  Row(

                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: LaminationDeliverDate==null? Text("Lamination Delivey  Date",style: TextStyle(color: Colors.grey.shade400),):Text("${LaminationDeliverDate!.day}/${LaminationDeliverDate!.month}/${LaminationDeliverDate!.year}"),
                        ),
                        GestureDetector(
                          onTap: () async {
                            LaminationDeliverDate = await   _selectDate( context);
                            setState(() {

                            });
                          },
                          child: const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(Icons.date_range,color: Colors.blue,),
                          ),
                        )


                      ]),
                ],
              ),
            ),
          ):const SizedBox.shrink(),
          role.elementAt(0).toString()=="Store"? ElevatedButton(onPressed: (){
            StoreUpdateRequest store =    StoreUpdateRequest(orderId: widget.order_id,board: board,inks: inks,lamination: lamination,varnish: varnish,

              boardDetailsType: board?'Avaliable':'Ordered',

              inksDetailsType: inks?'Avaliable':'Ordered',
              laminationDetailsType:  lamination?'Avaliable':'Ordered',
              varnishDetailsType:   varnish?'Avaliable':'Ordered',




            );
            if(!board && BoarddDeliverDate==null){

            MyWidgets.showSnakbarMsg(context, "Please Pick Board Delivery Date");
            return;
            }else if(BoarddDeliverDate !=null){
              store.boardDetailsDate =  BoarddDeliverDate.toString();
            }
            if(!inks && InksDeliverDate ==null){

              MyWidgets.showSnakbarMsg(context, "Please Pick Inks Delivery Date");
              return;
            }
            else if(InksDeliverDate!=null){
              store.inksDetailsDate = InksDeliverDate.toString();
            }
               if(!varnish && VarnishDeliverDate ==null){

                 MyWidgets.showSnakbarMsg(context, "Please Pick Varnish Delivery Date");
              return ;
            }
            else if(VarnishDeliverDate!=null){
                 store.varnishDetailsDate = VarnishDeliverDate.toString();
            }
              if(!lamination && LaminationDeliverDate ==null){


                MyWidgets.showSnakbarMsg(context, "Please Pick Lamination Delivery Date");
               return;
            }
            else  if(LaminationDeliverDate !=null){
                store.laminationDetailsDate = LaminationDeliverDate.toString();
            }


 /*       DateTime?  boardDetailsDate=board ==false? BoarddDeliverDate:null;
           DateTime? inksDetailsDate= inks ==false?InksDeliverDate:null;
           DateTime? laminationDetailsDate= lamination==false? LaminationDeliverDate:null;
          DateTime?  varnishDetailsDat=  varnish==false ?  VarnishDeliverDate:null;


*/

            logger.i(store.toJson().toString());
            ApiProvider(context).postStore(store).then((value){

              MyWidgets.showSnakbarMsg(context, "Store Updated");


            }).catchError((Object obj){
              MyWidgets.showSnakbarMsg(context, obj.toString());

            });







          }, child: const Text("Update")):SizedBox.shrink()

        ]),
      ),
    );
  }


  Future<DateTime> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));


    setState(() {

    });

    return picked!;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.store!=null){

      board = widget.store!.board!;
      inks = widget.store!.inks!;
      varnish = widget.store!.varnish!;
      lamination = widget.store!.lamination!;

  //  BoarddDeliverDate =widget.store!.boardDetails==null?null:widget.store!.boardDetails!.deliveryDate!;
 //     InksDeliverDate =widget.store!.inkDetails!.deliveryDate!;
 //     VarnishDeliverDate=widget.store!.varnishDetails!.deliveryDate!;
 //   LaminationDeliverDate = widget.store!.laminationDetails!.deliveryDate!;

       if(!widget.store!.board! &&widget.store!.boardDetails!=null){

         BoarddDeliverDate = widget.store!.boardDetails!.deliveryDate;

       }if(!widget.store!.inks! &&widget.store!.inkDetails!=null){

         InksDeliverDate = widget.store!.inkDetails!.deliveryDate;

       }if(!widget.store!.varnish! &&widget.store!.varnishDetails!=null){

         VarnishDeliverDate = widget.store!.varnishDetails!.deliveryDate;

       }if(!widget.store!.lamination! &&widget.store!.laminationDetails!=null){

         LaminationDeliverDate = widget.store!.laminationDetails!.deliveryDate;

       }

    setState(() {

    });


    }
  }
}
