

import 'dart:io';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/main.dart';

import '../Provider/UserProvider.dart';

class DisptchScreen extends StatefulWidget {


  String order_id;
   DisptchScreen({Key? key, required  this.order_id}) : super(key: key);

  @override
  State<DisptchScreen> createState() => _DisptchScreenState();
}

class _DisptchScreenState extends State<DisptchScreen> {

  List<FilePickerResult> images=[];
  List<FilePickerResult> invoices=[];
  @override
  Widget build(BuildContext context) {

    List? role = context.read<UserProvider>().userJwtResponse.permisions;

    print(    role!.elementAt(0).toString());



    return Column(children: [


      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

          Text("Upload Images"),
      images.length!=3?    GestureDetector(onTap: () async {

            await FilePicker.platform.pickFiles(
              type: FileType.custom,
              allowedExtensions: ['jpg', 'pdf', 'doc'],
            ).then((value) {

              images.add(value!);


            });

            setState(() {

            });
          },child: Container(width: 25,height: 25,color: Colors.grey.shade300,child: Center(child: Icon(Icons.add,size: 20,color: Colors.blue,)))):SizedBox.shrink(),


        ],),
      ),
      Divider(height: 5,),
      images.isNotEmpty?  SizedBox(
        height: 70,
        child:    ListView.builder(scrollDirection: Axis.horizontal,shrinkWrap: true,itemCount: images.length,itemBuilder: (context,index){

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(children: [

              SizedBox(child: Image.file(File(images.elementAt(index).files.first.path.toString()),height: 150,width: 150,),),
              // Positioned(right: 0,top: 0,child: Container( width: 30,decoration:  BoxDecoration(
              //   color: Colors.grey,
              //   shape: BoxShape.circle,
              // ),child: Center(child: IconButton(onPressed: (){}, icon: Icon(Icons.close,color: Colors.black,size: 20,))))),

            ],),
          );



        }),
      ):Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(height: 70,width: 120,color: Colors.grey.shade400,child: Center(child:Text("Upload Images")),),
      ),
      


      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            Text("Upload Invoice"),
       invoices.length!=3?       GestureDetector(onTap: () async {

              await FilePicker.platform.pickFiles(
                type: FileType.custom,
                allowedExtensions: ['jpg', 'pdf', 'doc'],
              ).then((value) {

                invoices.add(value!);


              });
              setState(() {

              });
            },child: Container(width: 25,height: 25,color: Colors.grey.shade300,child: Center(child: Icon(Icons.add,size: 20,color: Colors.blue,)))):SizedBox.shrink(),



          ],),
      ),

      Divider(height: 5,),

     invoices.isNotEmpty?  SizedBox(
        height: 70,
        child:

        ListView.builder(scrollDirection: Axis.horizontal,shrinkWrap: true,itemCount: invoices.length,itemBuilder: (context,index){

          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(children: [

              SizedBox(child: Image.file(File(invoices.elementAt(index).files.first.path.toString()),height: 150,width: 150,),),
              // Positioned(right: 0,top: 0,child: Container( width: 30,decoration:  BoxDecoration(
              //   color: Colors.grey,
              //   shape: BoxShape.circle,
              // ),child: Center(child: IconButton(onPressed: (){}, icon: Icon(Icons.close,color: Colors.black,size: 20,))))),

            ],),
          );



        }),
      ):Padding(
       padding: const EdgeInsets.all(8.0),
       child: Padding(
         padding: const EdgeInsets.all(8.0),
         child: Container(height: 70,width: 120,color: Colors.grey.shade400,child: Center(child:Text("Upload Invoice")),),
       ),
     ),


      role.elementAt(0).toString()=="Dispatcher"?   ElevatedButton(onPressed: () async {


        List<MultipartFile> uploadimages =[];
        List<MultipartFile> uploadInvouces =[];

        images.forEach((element) async {

         uploadimages.add(

              await MultipartFile.fromFile(element.files.first.path!, filename: element.files.first.path!.substring(element.files.first.path!.lastIndexOf("/") + 1, element.files.first.path!.length)),
          );

          logger.d(element.files.first.path!);
        });

        invoices.forEach((element) async {

          uploadInvouces.add(

              await MultipartFile.fromFile(element.files.first.path!, filename: element.files.first.path!.substring(element.files.first.path!.lastIndexOf("/") + 1, element.files.first.path!.length)),
          );

          logger.d(element.files.first.path!);
          logger.d(uploadimages.length);
          logger.d(uploadInvouces.length);
        });


        logger.d(uploadimages.length);
        logger.d(uploadInvouces.length);

        Dio dio = Dio();
Future.delayed( const Duration(seconds: 1),() async{

  var formData = FormData.fromMap({
    'name': 'wendux',
    'age': 25,
    'photos': uploadimages,
    'user':   uploadInvouces

    //  'file': await MultipartFile.fromFile(file.path, filename: file.path.substring(file.path.lastIndexOf("/") + 1, file.path.length)),
    //   'files': [
    //     await MultipartFile.fromFile('./text1.txt', filename: 'text1.txt'),
    //     await MultipartFile.fromFile('./text2.txt', filename: 'text2.txt'),
    //   ]
  });



  logger.d(uploadimages.length);
  logger.d(uploadInvouces.length);
  await dio.post('http://walnut.redmatter.tech/orders/postDispatch/${widget.order_id}', data: formData).then((value) {

    if(value.statusCode==200){

      MyWidgets.showSnakbarMsg(context, "Dispatch Updated");

    }else if(value.statusCode==500){

      MyWidgets.showSnakbarMsg(context, "Dispatch Not  Updated");

    }
    logger.d(value.toString());

  });



});

      }, child: Text("FINISH")):SizedBox.shrink()
    ],);
  }
}
