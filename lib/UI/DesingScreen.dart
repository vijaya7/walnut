
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Models/Request/DesignUpdateRequest.dart';
import 'package:rmt_walnut/Models/Response/GetOrderbyIdResponse.dart';
import 'package:rmt_walnut/Provider/UserProvider.dart';
import 'package:rmt_walnut/Utils/MyConst.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

class DesignScreen extends StatefulWidget {

  Design? desing;
    String order_id;

   DesignScreen({Key? key,this.desing,required this.order_id}) : super(key: key);

  @override
  State<DesignScreen> createState() => _DesignScreenState();
}

class _DesignScreenState extends State<DesignScreen> {

  DateTime? PoDispatchDate;
  DateTime? DyeDate;

  String? plates  ;
  // List of items in our dropdown menu
  var platesItems = ['Ordered', 'DesignFilePending',];
  String? dye  ;
  // List of items in our dropdown menu
  var dyeitems = ['Keyline pending','Quote pending', 'Ordered',];

  String? positives  ;
  // List of items in our dropdown menu
  var positivesitems =['FBB', 'GreyBlack', 'White Back',];

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;


    List? role = context.read<UserProvider>().userJwtResponse.permisions;

  print(    role!.elementAt(0).toString());


    return SingleChildScrollView(child: Column(children: [


      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(


          //    margin: const EdgeInsets.all(15.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  DropdownButton(

            // Initial Value
            value: plates,
            underline: const SizedBox(),
            hint: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Plates",style: TextStyle(color: Colors.grey.shade400),),
            ),
            isExpanded: true,
            onTap: (){

            },
            // Down Arrow Icon
            icon: const Icon(Icons.keyboard_arrow_down),

            // Array list of items
            items: platesItems.map((String items) {
              return DropdownMenuItem(
                value: items,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(items),
                ),
              );
            }).toList(),

            // After selecting the desired option,it will
            // change button value to selected value
            onChanged: role.elementAt(0).toString()=="Designer"? (String? newValue) {
              setState(() {
                plates = newValue!;
              });
            }:null,

          ),
        ),
      ),

    plates!=null && plates =="Ordered" ?  Padding(
        padding: const EdgeInsets.all(8.0),


        child:     Container(


          height: 60,
          //   margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  Row(

              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: PoDispatchDate==null? Text("Delivered Date",style: TextStyle(color: Colors.grey.shade400)):Text(PoDispatchDate!.day.toString()+"/"+PoDispatchDate!.month.toString()+"/"+PoDispatchDate!.year.toString()),

                ),
                GestureDetector(
                  onTap: () async {


                    PoDispatchDate = await   _selectDate( context);
                    setState(() {

                    });


                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.date_range,color: Colors.blue,),
                  ),
                )


              ]),
        ),
      ):const SizedBox.shrink(),

      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(


          //    margin: const EdgeInsets.all(15.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  DropdownButton(


            // Initial Value
            value: dye,
            underline: const SizedBox(),
            hint: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Dye",style: TextStyle(color: Colors.grey.shade400),),
            ),
            isExpanded: true,
            // Down Arrow Icon
            icon: const Icon(Icons.keyboard_arrow_down),

            // Array list of items
            items: dyeitems.map((String items) {
              return DropdownMenuItem(
                value: items,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(items),
                ),
              );
            }).toList(),
            // After selecting the desired option,it will
            // change button value to selected value
            onChanged:role.elementAt(0).toString()=="Designer"? (String? newValue) {
              setState(() {
                dye = newValue!;
              });
            }:null,
          ),
        ),
      ),
      dye!=null && dye =="Ordered" ?  Padding(
        padding: const EdgeInsets.all(8.0),


        child:     Container(


          height: 60,
          //   margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  Row(

              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DyeDate==null? Text("Delivered Date",style: TextStyle(color: Colors.grey.shade400)):Text(DyeDate!.day.toString()+"/"+DyeDate!.month.toString()+"/"+DyeDate!.year.toString()),

                ),
                GestureDetector(
                  onTap: () async {


                    DyeDate = await   _selectDate( context);
                    setState(() {

                    });


                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.date_range,color: Colors.blue,),
                  ),
                )


              ]),
        ),
      ):const SizedBox.shrink(),

      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(


          //    margin: const EdgeInsets.all(15.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  DropdownButton(


            // Initial Value
            value: positives,
            underline: const SizedBox(),
            hint: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Positives",style: TextStyle(color: Colors.grey.shade400),),
            ),
            isExpanded: true,
            // Down Arrow Icon
            icon: const Icon(Icons.keyboard_arrow_down),

            // Array list of items
            items: positivesitems.map((String items) {
              return DropdownMenuItem(
                value: items,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(items),
                ),
              );
            }).toList(),
            // After selecting the desired option,it will
            // change button value to selected value
            onChanged:role.elementAt(0).toString()=="Designer"? (String? newValue) {
              setState(() {
                positives = newValue!;
              });
            }:null,
          ),
        ),
      ),

      role.elementAt(0).toString()=="Designer"?   ElevatedButton(onPressed: (){

        if(plates!=null && dye !=null && positives !=null){

        if(plates =="Ordered" && PoDispatchDate!=null  &&dye =="Ordered" && DyeDate!=null  && positives!=null){
             print("Done");

             ApiProvider(context).postDesign(DesignUpdateRequest(orderId: widget.order_id,dye: dye,dyeDeliverDate: DyeDate.toString(),platesDeliverDate: PoDispatchDate.toString(),plates: plates,positives: positives )).then((value) {


               MyWidgets.showSnakbarMsg(context, "Design Status Updated");

             });

        }



        else if(plates !="Ordered"   &&dye !="Ordered" &&  positives!=null){


           ApiProvider(context).postDesign(DesignUpdateRequest(orderId: widget.order_id,dye: dye,plates: plates,positives: positives,dyeDeliverDate: DyeDate.toString(),platesDeliverDate: PoDispatchDate.toString())).then((value) {


            MyWidgets.showSnakbarMsg(context, "Design Status Updated");

          });


        }


        else if(plates=="Ordered" && PoDispatchDate!=null  &&dye !="Ordered" &&  positives!=null){
          ApiProvider(context).postDesign(DesignUpdateRequest(orderId: widget.order_id,dye: dye,plates: plates,positives: positives,dyeDeliverDate: DyeDate.toString(),platesDeliverDate: PoDispatchDate.toString())).then((value) {


            MyWidgets.showSnakbarMsg(context, "Design Status Updated");

          });

        }
        else if(plates!="Ordered"  &&dye =="Ordered" && DyeDate!=null &&  positives!=null){
          ApiProvider(context).postDesign(DesignUpdateRequest(orderId: widget.order_id,dye: dye,plates: plates,positives: positives,dyeDeliverDate: DyeDate.toString(),platesDeliverDate: PoDispatchDate.toString())).then((value) {


            MyWidgets.showSnakbarMsg(context, "Design Status Updated");

          });

        }



        }

        else if(plates ==null){

          MyWidgets.showSnakbarMsg(context,"Please Select Plates Type" );
          return;
        } else if(plates =="Ordered" && PoDispatchDate==null){

        MyWidgets.showSnakbarMsg(context,"Please Select Plates Delivery Date" );

        return;
        }


        else if(dye ==null){

          MyWidgets.showSnakbarMsg(context,"Please Select Dye Type" );
        }
        else   if(dye =="Ordered" && DyeDate==null){

        MyWidgets.showSnakbarMsg(context,"Please Select Dye Delivery Date" );
        return;
        }


        else{
          MyWidgets.showSnakbarMsg(context,"Some Feilds Required" );
        }



      }, child: const Padding(
        padding: EdgeInsets.all(8.0),
        child: Text("Update"),
      )):SizedBox.shrink(),


    ]),);
  }

  Future<DateTime> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));


    setState(() {

    });

    return picked!;
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();


   // print(widget.desing!.toJson().toString());

    if(widget.desing!=null){


      plates = widget.desing!.plates;
      dye = widget.desing!.dye;
      positives = widget.desing!.positives;

      PoDispatchDate= DateTime.tryParse(widget.desing!.platesDeliverDate.toString());
       DyeDate = DateTime.tryParse(widget.desing!.dyeDeliveryDate.toString());

      setState(() {

      });



    }
  }
}
