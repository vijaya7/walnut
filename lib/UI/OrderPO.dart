import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rmt_walnut/Models/Request/OrderPost.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

class OderPage extends StatefulWidget {
  const OderPage({Key? key}) : super(key: key);

  @override
  State<OderPage> createState() => _OderPageState();
}

class _OderPageState extends State<OderPage> {



   final _formKey = GlobalKey<FormState>();
   DateTime? Poreceiveddate;
   DateTime? PoDispatchDate;
   TextEditingController projectname = TextEditingController();
   TextEditingController customername = TextEditingController();
   TextEditingController ponumber = TextEditingController();
  // TextEditingController poreceiveddate = TextEditingController();
   TextEditingController dispatchdate = TextEditingController();
   TextEditingController orderquantity = TextEditingController();


   String? boradvalue  ;
  // List of items in our dropdown menu
  var boardItems = ['FBB', 'GreyBlack','White_back','GSM_Type' ,'Code_Type',];

  String? inksvalue  ;

  // List of items in our dropdown menu
  var inksItems =  ['CMKY', 'Pantone','Code_Type'];

  String? varnishvalue  ;

  // List of items in our dropdown menu
  var varnishItems = ['VU', 'Aqua', 'Primer','Lacker'];

  String? laminationvalue  ;

  // List of items in our dropdown menu
  var laminationItems = ['Gloss', 'Matt'];



   DateTime selectedDate = DateTime.now();

   Future<DateTime> _selectDate(BuildContext context) async {
     final DateTime? picked = await showDatePicker(
         context: context,
         initialDate: selectedDate,
         firstDate: DateTime(2015, 8),
         lastDate: DateTime(2101));


     setState(() {

     });

      return picked!;
   }
  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return Scaffold(appBar: AppBar(centerTitle: true,elevation: 0,backgroundColor: Colors.transparent,title: Text("Project Name",style: TextStyle(color: Colors.blue,fontSize: 15),),
        leading: GestureDetector(onTap: (){

          Navigator.pop(context);

        },child: Icon(Icons.arrow_back,color: Colors.blue,))),

    body: Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: _formKey,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(children: [

            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: (text){
                  if(text!.isEmpty){
                    return "Order Name should not be empty";
                  }else{
                    return null;
                  }
                },
                controller: projectname,
                decoration:InputDecoration(
                  labelText: "Project Name",
                  labelStyle: TextStyle( color: Colors.grey.shade500),
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.indigo,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red.shade300,
                      width: 2.0,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.grey.shade300,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: customername,
                validator: (text){
                  if(text!.isEmpty){
                    return "Customer Name should not be empty";
                  }else{
                    return null;
                  }
                },
                decoration:InputDecoration(
                  labelText: "Customer Name",
                  labelStyle: TextStyle( color: Colors.grey.shade500),
                  fillColor: Colors.white,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.blue,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red.shade300,
                      width: 2.0,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.grey.shade300,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: ponumber,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly
                ],
                validator: (text){

                  if(text!.isEmpty){


                    return "PO Number should not be empty";
                  }else{


                    return null;
                  }

                },
                decoration:InputDecoration(
                  labelText: "PO Number",
                  labelStyle: TextStyle( color: Colors.grey.shade500),
                  fillColor: Colors.white,

                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.blue,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.red.shade300,
                      width: 2.0,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                    borderSide: BorderSide(
                      color: Colors.grey.shade300,
                      width: 2.0,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


            height: 50,
             //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(

                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Poreceiveddate==null? Text("PO Received Date",style: TextStyle(color: Colors.grey.shade400),):Text(Poreceiveddate!.day.toString()+"/"+Poreceiveddate!.month.toString()+"/"+Poreceiveddate!.year.toString()),
                  ),
                  GestureDetector(
                    onTap: () async {
                      Poreceiveddate = await   _selectDate( context);
                      setState(() {

                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.date_range,color: Colors.blue,),
                    ),
                  )


                ]),
              ),
            ),

            // TextFormField(
            //
            //   decoration:InputDecoration(
            //     labelText: "Enter Email",
            //     suffixIcon: Icon(Icons.date_range,color: Colors.blue,),
            //
            //
            //     suffixIconColor: Colors.blue,
            //
            //
            //     labelStyle: TextStyle( color: Colors.grey.shade500),
            //     fillColor: Colors.white,
            //
            //     focusedBorder: OutlineInputBorder(
            //       borderRadius: BorderRadius.circular(15.0),
            //       borderSide: BorderSide(
            //         color: Colors.blue,
            //       ),
            //     ),
            //     enabledBorder: OutlineInputBorder(
            //       borderRadius: BorderRadius.circular(15.0),
            //       borderSide: BorderSide(
            //         color: Colors.grey.shade300,
            //         width: 2.0,
            //       ),
            //     ),
            //
            //   ),
            // )
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 50,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(

                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [

                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: PoDispatchDate==null? Text("Dispatch Date",style: TextStyle(color: Colors.grey.shade400)):Text(PoDispatchDate!.day.toString()+"/"+PoDispatchDate!.month.toString()+"/"+PoDispatchDate!.year.toString()),

                      ),
                      GestureDetector(
                        onTap: () async {


                            PoDispatchDate = await   _selectDate( context);
                            setState(() {

                            });


                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.date_range,color: Colors.blue,),
                        ),
                      )


                    ]),
              ),
            ),
            Align(alignment: Alignment.centerLeft,child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Specification",style: TextStyle(color: Colors.blue),),
            )),

            Row(children: [
              SizedBox(
                width: size.width*0.5,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: orderquantity,
                    validator: (text){

                      if(text!.isEmpty){


                        return "Quality should not be empty";
                      }else{


                        return null;
                      }

                    },
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    decoration:InputDecoration(
                      labelText: "Order Quantity",


                      suffixIconColor: Colors.blue,

                      labelStyle: TextStyle( color: Colors.grey.shade400),
                      fillColor: Colors.white,

                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          color: Colors.blue,
                        ),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          color: Colors.red.shade300,
                          width: 2.0,
                        ),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          color: Colors.red,
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          color: Colors.grey.shade200,
                          width: 2.0,
                        ),
                      ),

                    ),
                  ),
                ),
              ),


              Container(
                width: size.width/2.2,
                height: 60,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(


                  // Initial Value
                  value: boradvalue,
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Board",style: TextStyle(color: Colors.grey.shade400),),
                  ),
                  isExpanded: true,
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: boardItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) {
                    setState(() {
                      boradvalue = newValue!;
                    });
                  },
                ),
              )



            ],),

            Row(crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.spaceAround,children: [
              Container(

                width: size.width/2.2,
                height: 60,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:    DropdownButton(


                  // Initial Value
                  value: inksvalue,
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Inks",style: TextStyle(color: Colors.grey.shade400),),
                  ),
                  isExpanded: true,
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: inksItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) {
                    setState(() {
                      inksvalue = newValue!;
                    });
                  },
                ),
              ),
              Container(
                width: size.width/2.2,
                height: 60,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(


                  // Initial Value
                  value: varnishvalue,
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Varnish",style: TextStyle(color: Colors.grey.shade400),),
                  ),
                  isExpanded: true,
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: varnishItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) {
                    setState(() {
                      varnishvalue = newValue!;
                    });
                  },
                ),
              )

            ],),
           Row(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.start,children: [
              Container(

                width: size.width/2.2,
                height: 60,
                    margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:    DropdownButton(


                  // Initial Value
                  value: laminationvalue,

                  isExpanded: true,
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Lamination",style: TextStyle(color: Colors.grey.shade400),),
                  ),
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: laminationItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (String? newValue) {
                    setState(() {
                      laminationvalue = newValue!;
                    });
                  },
                ),
              ),


            ],),


            ElevatedButton(style: ElevatedButton.styleFrom(backgroundColor: Colors.blue),onPressed: (){

              if(_formKey.currentState!.validate()&&Poreceiveddate!=null&&PoDispatchDate!=null&& boradvalue!=null && inksvalue!=null &&varnishvalue!=null&& laminationvalue!=null){


                ApiProvider(context).postOrder(OrderPostRequest(customerName:customername.text,jobName: projectname.text,poNumber: ponumber.text,specBoardType: boradvalue,specInks: inksvalue,specLamination: laminationvalue,specQuantity: orderquantity.text,specVarnish: varnishvalue )).then((value) {


                  MyWidgets.showSnakbarMsg(context, "Order Placed");
                  Navigator.pop(context);


                }).catchError((Object obj){

                  MyWidgets.showSnakbarMsg(context, 'A Error Occured');
                });


              }else if(boradvalue==null){

                MyWidgets.showSnakbarMsg(context, "Please Select Board Value ");

              }else if(inksvalue==null){

                MyWidgets.showSnakbarMsg(context, "Please Select Inks Value ");

              }
              else if(varnishvalue==null){

                MyWidgets.showSnakbarMsg(context, "Please Select Varnish Value ");

              }  else if(laminationvalue==null){

                MyWidgets.showSnakbarMsg(context, "Please Select Lamination Value ");

              } else if(Poreceiveddate==null){

                MyWidgets.showSnakbarMsg(context, "Please Select PO Received Date  ");

              }  else if(PoDispatchDate==null){

                MyWidgets.showSnakbarMsg(context, "Please Select Po Dispatch Date");

              }



            }, child: SizedBox(width: size.width*0.8,child: Center(child: Text("Submit"))))
          ]),
        ),
      ),
    ),



    );
  }
}
