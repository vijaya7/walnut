import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Models/Response/OrderStats.dart';
import 'package:rmt_walnut/Utils/MyConst.dart';
import 'package:rmt_walnut/main.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

import '../Models/Response/GetOrderRespsonse.dart';
import '../Provider/UserProvider.dart';
import 'OrderPO.dart';
import 'OrderStatusUpdate.dart';
class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Color color = const Color.fromRGBO(55, 126, 192, 1);
  int selectedIndex = 0;

late  Future<GetOrdersResponse> getOrderResponse;
late  Future<Orderstats> orderStats;
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    List? role = context.read<UserProvider>().userJwtResponse.permisions;

   // print(    role!.elementAt(0).toString());
    return Scaffold(


      key: _key,
       floatingActionButton:  role!.elementAt(0).toString()=="Admin" ?   GestureDetector(onTap: () async {

     await   Navigator.of(context).push(MaterialPageRoute(builder: (context)=>const OderPage()));

     getOrderResponse= ApiProvider(context).getOrder("Design");
     orderStats = ApiProvider(context).getStats();

     setState(() {

     });

      },child: Container(width: 55,height: 55,decoration:  BoxDecoration(shape: BoxShape.circle,color: color,),child: const Center(child: Icon(Icons.add)))):SizedBox.shrink(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
      appBar: AppBar(backgroundColor: color,centerTitle: true,title: Text("Welcome"),actions: [IconButton(onPressed: () async {

        await Hive.box(final_box).delete("access_Token");
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyHomePage(title: "")));



      }, icon: Icon(Icons.output_rounded))]),

      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: selectedIndex,
        onTap: (index) {

          if(index==0 || index ==3){

            selectedIndex = index;

            setState(() {});
          }

        },
        selectedItemColor: color,
        selectedLabelStyle: TextStyle(color: color),
        //      selectedIconTheme: IconThemeData(color: Colors.orangeAccent),



        backgroundColor: Colors.white,
        unselectedItemColor: color,
        items:  [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                 color: color,
              ),
              label: "Home",
              activeIcon: Icon(Icons.home,color: color,)
          ),
          BottomNavigationBarItem(
              activeIcon: Icon(CupertinoIcons.search,color: color,),
              icon: const Icon(CupertinoIcons.search), label: "Search"),

          BottomNavigationBarItem(
              activeIcon: Icon(Icons.notifications_rounded,color: color,),
              icon: const Icon(Icons.notifications_rounded),
              label: "Notifications"),
          BottomNavigationBarItem(
              activeIcon: Icon(Icons.account_circle,color: color,),
              icon: const Icon(Icons.account_circle),
              label: "Account"),
        ],
      ),
      // drawer: Drawer(child: Column(children: [
      //
      //   DrawerHeader(decoration: BoxDecoration(color: color),child: SizedBox(width: size.width,child: Text("")),curve: Curves.bounceIn),
      //
      //   GestureDetector(child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //      crossAxisAlignment: CrossAxisAlignment.center,
      //     children: [
      //       Icon(Icons.lock),
      //       Text("Logout"),
      //     ],
      //   ),onTap: () async {
      //
      //
      //     await Hive.box(final_box).delete("access_Token");
      //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyHomePage(title: "")));
      //
      //   },)
      //
      // ],)),
      body: IndexedStack(
        index: selectedIndex,
        children: [

          Column(children: [

            Container(height: 100,width: size.width,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),child:


            FutureBuilder(future: orderStats, builder: (BuildContext context, AsyncSnapshot<Orderstats> snapshot) {

              if(snapshot.connectionState == ConnectionState.done){

                return   ListView.builder(shrinkWrap: true,scrollDirection: Axis.horizontal,itemCount: snapshot.data!.result!.length,itemBuilder: (context,index){


                  logger.d(snapshot.data!.result!.elementAt(index).toJson().toString());
                  return GestureDetector(
                    onTap: (){


                      getOrderResponse= ApiProvider(context).getOrder(snapshot.data!.result!.elementAt(index).staus.toString());
                      orderStats = ApiProvider(context).getStats();

                      setState(() {

                      });
                      print(snapshot.data!.result!.elementAt(index).staus.toString());
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(child: Center(child: Text(snapshot.data!.result!.elementAt(index).count.toString(),style: TextStyle(fontWeight: FontWeight.bold),)),height:50,width: 75,decoration: BoxDecoration(color: Colors.grey.shade300,borderRadius: BorderRadius.circular(15))),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(snapshot.data!.result!.elementAt(index).staus.toString(),style: TextStyle(fontWeight: FontWeight.normal,color: color,fontSize: 15),),
                          )
                        ],
                      ),
                    ),
                  );

                });

              }


              else {

                return const Center(child: CircularProgressIndicator(),);
              }

            },)

            ),

            // Stack(
            // children: [
            //
            //
            //
            //   // Positioned(
            //   //   height: size.height*0.3,
            //   //   width: size.width,
            //   //   top: 50,
            //   //
            //   //   child: Column(children: [
            //   //
            //   //     Padding(
            //   //       padding: const EdgeInsets.all(8.0),
            //   //       child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, crossAxisAlignment: CrossAxisAlignment.center,children:  [
            //   //
            //   //         GestureDetector(onTap: (){
            //   //
            //   //           _key.currentState!.openDrawer();
            //   //
            //   //
            //   //         },child: Icon(Icons.menu_outlined,color: Colors.white,)),
            //   //         Icon(Icons.notification_add_rounded,color: Colors.white,),
            //   //
            //   //       ],),
            //   //     ),
            //   //     Padding(
            //   //       padding: const EdgeInsets.all(8.0),
            //   //       child: TextFormField(decoration: const InputDecoration(border: InputBorder.none,
            //   //           labelStyle: TextStyle(color: Colors.white),
            //   //           prefixIcon: Icon(Icons.search,color: Colors.white,),label: Text("Search")),),
            //   //     )
            //   //
            //   //
            //   //
            //   //   ]),
            //   // ),
            //   // Positioned(top: 140,child: Container(height: 100,width: size.width,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),child:
            //   //
            //   //
            //   //     FutureBuilder(future: orderStats, builder: (BuildContext context, AsyncSnapshot<Orderstats> snapshot) {
            //   //
            //   //      if(snapshot.connectionState == ConnectionState.done){
            //   //
            //   //        return   ListView.builder(shrinkWrap: true,scrollDirection: Axis.horizontal,itemCount: snapshot.data!.result!.length,itemBuilder: (context,index){
            //   //
            //   //          return GestureDetector(
            //   //            onTap: (){
            //   //
            //   //
            //   //              getOrderResponse= ApiProvider().getOrder(snapshot.data!.result!.elementAt(index).staus.toString());
            //   //              orderStats = ApiProvider().getStats();
            //   //
            //   //              setState(() {
            //   //
            //   //              });
            //   //              print(snapshot.data!.result!.elementAt(index).staus.toString());
            //   //            },
            //   //            child: Padding(
            //   //              padding: const EdgeInsets.all(8.0),
            //   //              child: Column(
            //   //                children: [
            //   //                  Container(child: Center(child: Text(snapshot.data!.result!.elementAt(index).count.toString(),style: TextStyle(fontWeight: FontWeight.bold),)),height:50,width: 75,decoration: BoxDecoration(color: Colors.grey,borderRadius: BorderRadius.circular(15))),
            //   //                  Padding(
            //   //                    padding: const EdgeInsets.all(8.0),
            //   //                    child: Text(snapshot.data!.result!.elementAt(index).staus.toString(),style: TextStyle(color: Colors.white,fontSize: 10),),
            //   //                  )
            //   //                ],
            //   //              ),
            //   //            ),
            //   //          );
            //   //
            //   //        });
            //   //
            //   //      }
            //   //
            //   //
            //   //      else {
            //   //
            //   //        return const Center(child: CircularProgressIndicator(),);
            //   //      }
            //   //
            //   //     },)
            //   //
            //   // )),
            //
            //
            //
            // ],
            // ),


            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text("Projects",style: TextStyle(fontStyle: FontStyle.italic,fontWeight: FontWeight.bold),),
            ),
            FutureBuilder(future: getOrderResponse, builder: (BuildContext context, AsyncSnapshot<GetOrdersResponse> snapshot) {


              if(snapshot.connectionState == ConnectionState.waiting){


                return const Center(child:  CircularProgressIndicator(),);
              }   if(snapshot.connectionState == ConnectionState.done){

                if(snapshot.hasData){

                  return Expanded(
                    child: ListView.builder(itemCount: snapshot.data!.result!.length,shrinkWrap: true,itemBuilder: (context,index){

                      return GestureDetector(

                        onTap: (){



                          Navigator.of(context).push(MaterialPageRoute(builder: (context)=>OrderStatusUpdate(order_id: snapshot.data!.result!.elementAt(index).id.toString(),projectName: snapshot.data!.result!.elementAt(index).jobName.toString())));




                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.grey.shade200),
                            height: size.height/7,child: Row(
                              children: [

                                Flexible(
                                  flex: 6,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(

                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Spacer(),
                                        Text(snapshot.data!.result!.elementAt(index).jobName.toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                                        Spacer(),
                                        Row(
                                          children: [
                                            Text("Customer Name :",style: TextStyle(fontStyle: FontStyle.italic,color: Colors.grey.shade600)),
                                            Text(snapshot.data!.result!.elementAt(index).customerName.toString(),style: TextStyle(fontStyle: FontStyle.italic,color: Colors.grey.shade600)),
                                          ],
                                        ),
                                        Flexible(
                                          child: Row(
                                            children: [
                                              Text("Po Number:",style: TextStyle(fontStyle: FontStyle.italic,color: Colors.grey.shade600)),
                                              Text(snapshot.data!.result!.elementAt(index).poNumber.toString(),style: TextStyle(fontStyle: FontStyle.italic,color: Colors.grey.shade600)),
                                            ],
                                          ),
                                        ),
                                        Spacer(),
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 4
                                  ,child: Column(

                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Spacer(),
                                    Container(height: 70,width: 75,decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),color: Colors.white70),child: Column(crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center,children: [

                                      Text(snapshot.data!.result!.elementAt(index).poDispatchDate!.day.toString(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.blue),),
                                      Text(MyConst.getDate(snapshot.data!.result!.elementAt(index).poDispatchDate!.month),style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold,color: Colors.blue),),




                                    ]),),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text("Dispatch Date",style: TextStyle(fontSize: 9,color: Colors.blue,fontWeight: FontWeight.bold),),
                                    ),


                                    Spacer()
                                  ],
                                ),
                                ),
                              ],
                            ),),
                        ),
                      );

                    }),
                  );


                }
                return const Center(child:  CircularProgressIndicator(),);
              }else{
                return const Center(child:  CircularProgressIndicator(),);
              }



            },),

          ],),
          Container(),
          Container(),
          Column(

          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            const SizedBox(height:120,),
           // Container(height: 150,width: 50,decoration: BoxDecoration(shape: BoxShape.circle,color: Colors.grey.shade400),),

            Row(


              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [


              Text("User Name",style: TextStyle(fontWeight: FontWeight.bold),),
              Text(context.read<UserProvider>().userJwtResponse.phone.toString(),style: TextStyle(color: Colors.blue),)
            ],)  ,

          Row(


            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,

            children: [


            Text("Phone",style: TextStyle(fontWeight: FontWeight.bold)),
            Text(context.read<UserProvider>().userJwtResponse.phone.toString(),style: TextStyle(color: Colors.blue),)
          ],),

            Row(


              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,

              children: [


                Text("Role",style: TextStyle(fontWeight: FontWeight.bold)),
                Text(context.read<UserProvider>().userJwtResponse.permisions!.first.toString(),style: TextStyle(color: Colors.blue),)
              ],),




            Padding(
              padding:  EdgeInsets.only(left: size.width/2.5),
              child: GestureDetector(child: Row(



                children: [Icon(Icons.lock,color: Colors.blue,),
                  Text("Logout",style: TextStyle(color: Colors.blue),),
                ],
              ),onTap: () async {


                await Hive.box(final_box).delete("access_Token");
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>MyHomePage(title: "")));

              },),
            ),



            SizedBox(height: 60,)

          ])
        ],

      ),);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getOrderResponse= ApiProvider(context).getOrder("Design");
    orderStats = ApiProvider(context).getStats();
  }
}
