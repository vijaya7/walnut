
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Models/Response/GetOrderbyIdResponse.dart';
import 'package:rmt_walnut/Provider/UserProvider.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/main.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

import '../Models/Request/ProductionRequestModel.dart';
class ProductionScreen extends StatefulWidget {

  String order_id;
  Production? production;

   ProductionScreen({Key? key,this.production,required this.order_id}) : super(key: key);

  @override
  State<ProductionScreen> createState() => _ProductionScreenState();
}

class _ProductionScreenState extends State<ProductionScreen> {

  String printing ="Not_Started" ;
  String varnish ="Not_Started" ;
  String lamination  ="Not_Started";
  String punching ="Not_Started" ;
  String stripping = "Not_Started" ;
  String folding ="Not_Started" ;
  // List of items in our dropdown menu
 // var platesItems = ['Status', 'InProgress','Finished'];
  var platesItems = ['Not_Started','InProgress','Finished'];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    List? role = context.read<UserProvider>().userJwtResponse.permisions;

    print(    role!.elementAt(0).toString());

    return SingleChildScrollView(
      child: Column(children: [


        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          Padding(
          padding: const EdgeInsets.all(8.0),

        child:     Container(


          height: 40,
          width: size.width*0.3,
          //   margin: const EdgeInsets.all(10.0),
          padding: const EdgeInsets.all(3.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Colors.grey.shade300)
          ),
          child:  Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Spacer(),
              Icon(Icons.trip_origin, color: Colors.green, size: 10,),
              Text("PRINTING"),


              Spacer()
            ],
          ),
        ),
      )    ,
            SizedBox(height: 10,width: size.width*0.2,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: printing,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),


                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged:(String? newValue) {
                    if(newValue!="Finished"){
                      

                      varnish ="Not_Started";
                      lamination = "Not_Started";
                      punching = "Not_Started";
                      stripping = "Not_Started";
                      folding = "Not_Started";
                      
                      
                      
                    }

                    setState(() {
                      printing = newValue!;
                      
                    });
                    
                
                    
                  },

                ),
              ),
            ),

          ],),


       const Align(alignment: Alignment.centerLeft,child: SizedBox(height: 30,child: Padding(
         padding: EdgeInsets.only(left: 50),
         child: VerticalDivider(thickness: 3,width: 30,),
       ))),


        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 40,
                width: size.width*0.3,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Spacer(),
                    Icon(Icons.trip_origin, color: Colors.green, size: 10,),
                    Text("VARNISH"),


                    Spacer()
                  ],
                ),
              ),
            )    ,
            SizedBox(height: 10,width: size.width*0.2,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: varnish,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),

                  onTap: (){

                  },
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (printing=="Finished")?(String? newValue) {

                    if(newValue !="Finished"){



                      lamination = "Not_Started";
                      punching = "Not_Started";
                      stripping = "Not_Started";
                      folding = "Not_Started";



                    }

                    setState(() {
                      varnish = newValue!;
                    });
                  }:null,

                ),
              ),
            ),

          ],),

        const Align(alignment: Alignment.centerLeft,child: SizedBox(height: 30,child: Padding(
          padding: EdgeInsets.only(left: 50),
          child: VerticalDivider(thickness: 3,width: 30,),
        ))),
        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 40,
                width: size.width*0.3,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Spacer(),
                    Icon(Icons.trip_origin, color: Colors.green, size: 10,),
                    Text("LAMINTAION"),


                    Spacer()
                  ],
                ),
              ),
            )    ,
            SizedBox(height: 10,width: size.width*0.2,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: lamination,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),

                  onTap: (){

                  },
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged: (printing=="Finished" && varnish =="Finished")?(String? newValue) {


                    if(newValue !="Finished"){




                      punching = "Not_Started";
                      stripping = "Not_Started";
                      folding = "Not_Started";



                    }
                    setState(() {
                      lamination = newValue!;
                    });
                  }:null,
                ),
              ),
            ),

          ],),

        const Align(alignment: Alignment.centerLeft,child: SizedBox(height: 30,child: Padding(
          padding: EdgeInsets.only(left: 50),
          child: VerticalDivider(thickness: 3,width: 30,),
        ))),
        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 40,
                width: size.width*0.3,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Spacer(),
                    Icon(Icons.trip_origin, color: Colors.green, size: 10,),
                    Text("PUNCHING"),


                    Spacer()
                  ],
                ),
              ),
            )    ,
            SizedBox(height: 10,width: size.width*0.2,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: punching,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),

                  onTap: (){

                  },
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged:  (printing=="Finished" && varnish =="Finished"  && lamination =="Finished")?(String? newValue) {

                    if(newValue !="Finished"){





                      stripping = "Not_Started";
                      folding = "Not_Started";



                    }

                    setState(() {
                      punching = newValue!;
                    });
                  }:null,

                ),
              ),
            ),

          ],),

        const Align(alignment: Alignment.centerLeft,child: SizedBox(height: 30,child: Padding(
          padding: EdgeInsets.only(left: 50),
          child: VerticalDivider(thickness: 3,width: 30,),
        ))),
        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 40,
                width: size.width*0.4,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Spacer(),
                    Icon(Icons.trip_origin, color: Colors.green, size: 5,),
                    Text("STRIPING / DRESSING",),
                    Spacer()
                  ],
                ),
              ),
            )    ,
            SizedBox(height: 10,width: size.width*0.1,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: stripping,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),

                  onTap: (){

                  },
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged:  (printing=="Finished" && varnish =="Finished" &&punching =="Finished" && lamination =="Finished")?(String? newValue) {

                    if(newValue !="Finished"){






                      folding = "Not_Started";



                    }

      setState(() {
      stripping = newValue!;
      });
      }:null,


                ),
              ),
            ),

          ],),

        Align(alignment: Alignment.centerLeft,child: SizedBox(height: 30,child: Padding(
          padding: const EdgeInsets.only(left: 50),
          child: VerticalDivider(thickness: 3,width: 30,),
        ))),
        Row(

          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),


              child:     Container(


                height: 40,
                width: size.width*0.4,
                //   margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Spacer(),
                    Icon(Icons.trip_origin, color: Colors.green, size: 10,),
                    Text("FOLDING / GLUING"),


                    Spacer()
                  ],
                ),
              ),
            )    ,
            SizedBox(height: 10,width: size.width*0.1,child: Divider(thickness: 2,height: 20)),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(



                height: 50,
                //    margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Colors.grey.shade300)
                ),
                child:  DropdownButton(

                  // Initial Value
                  value: folding,
                  underline: const SizedBox(),
                  hint: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Status",style: TextStyle(color: Colors.grey.shade400),),
                  ),

                  onTap: (){

                  },
                  // Down Arrow Icon
                  icon: const Icon(Icons.keyboard_arrow_down),

                  // Array list of items
                  items: platesItems.map((String items) {
                    return DropdownMenuItem(
                      value: items,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(items),
                      ),
                    );
                  }).toList(),
                  // After selecting the desired option,it will
                  // change button value to selected value
                  onChanged:  (printing=="Finished" &&stripping =="Finished" && varnish =="Finished" &&punching =="Finished" && lamination =="Finished")?(String? newValue) {
                    setState(() {
                      folding = newValue!;
                    });
                  }:null,

                ),
              ),
            ),

          ],),


        role.elementAt(0).toString()=="Production"?  ElevatedButton(onPressed: (){
     

          ProductionUpdateRequest  production = ProductionUpdateRequest(orderId: widget.order_id,printing: printing, varnish:varnish , lamination: lamination, punching: punching, stripingDressing: stripping, foldingGuling: folding!);
          ApiProvider(context).postProdcution(production).then((value){

            
            MyWidgets.showSnakbarMsg(context, "Production Updated");
            
          });




        }, child: Text("Submit")):SizedBox.shrink(),




      ]),
    );
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.production!=null){



      printing = widget.production!.printing!;
      varnish = widget.production!.varnish!;
      lamination = widget.production!.lamination!;
      punching = widget.production!.punching!;
      stripping = widget.production!.stripingDressing!;
      folding = widget.production!.foldingGuling!;


      setState(() {

      });



    }
  }
}
