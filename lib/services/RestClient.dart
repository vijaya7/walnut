import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:rmt_walnut/Models/Request/DesignUpdateRequest.dart';
import 'package:rmt_walnut/Models/Request/OrderPost.dart';
import 'package:rmt_walnut/Models/Request/OtpRequest.dart';
import 'package:rmt_walnut/Models/Request/ProductionRequestModel.dart';
import 'package:rmt_walnut/Models/Request/StoreUpdateRequest.dart';
import 'package:rmt_walnut/Models/Request/UserLoginRequest.dart';
import 'package:rmt_walnut/Models/Response/GetOrderRespsonse.dart';
import 'package:rmt_walnut/Models/Response/OrderPostResponse.dart';
import 'package:rmt_walnut/Models/Response/UserLoginResponse.dart';
import '../Models/Request/OrderGetRequest.dart';
import '../Models/Response/GetOrderbyIdResponse.dart';
import '../Models/Response/OrderStats.dart';
import '../Models/Response/OtpVerifyResponse.dart';
import '../Models/Response/StoreUpdateresponse.dart';

part 'RestClient.g.dart';


//@RestApi(baseUrl: "http://10.0.2.2:3000/")
 @RestApi(baseUrl: "http://walnut.redmatter.tech/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("/user/user_login")
  Future<UserLoginResponse> userLogin(@Body() UserLoginRequest userLoginRequest);

  @POST("/user/verify_otp")
  Future<OtpVerifyResponse> userVerifyOtp(@Body() OtpVerifyRequest otpVerifyRequest);


  @POST("/orders/post_order")
  Future<OrderPostResponse> postOrder(@Body() OrderPostRequest orderPostRequest);

  @POST("/orders/get_orders")
  Future<GetOrdersResponse> getOrder( @Body() OrderstatsRequest orderstatsRequest );


  @GET("/orders/get_order_by_id/{id}")
  Future<OrderResponseById> getOrdersbyId( @Path() String id);


  @POST("/orders/changeDesignStatus")
  Future<OrderResponseById> postDesign(@Body() DesignUpdateRequest designUpdateRequest);

  @POST("/orders/changeStoreStatus")
  Future<StoreUpdateResponse> postStore(@Body() StoreUpdateRequest storeUpdateRequest);

  @POST("/orders/post_ptoduction")
  Future<String> postProdcution(@Body() ProductionUpdateRequest productionUpdateRequest);


  @GET("/orders/get_stats")
  Future<Orderstats> getStats();







}
