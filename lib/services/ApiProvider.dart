


import 'package:flutter/cupertino.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:rmt_walnut/Models/Request/DesignUpdateRequest.dart';
import 'package:rmt_walnut/Models/Request/OrderGetRequest.dart';
import 'package:rmt_walnut/Models/Request/OrderPost.dart';
import 'package:rmt_walnut/Models/Request/OtpRequest.dart';
import 'package:rmt_walnut/Models/Request/ProductionRequestModel.dart';
import 'package:rmt_walnut/Models/Request/StoreUpdateRequest.dart';
import 'package:rmt_walnut/Models/Request/UserLoginRequest.dart';
import 'package:rmt_walnut/Models/Response/GetOrderRespsonse.dart';
import 'package:rmt_walnut/Models/Response/OrderPostResponse.dart';
import 'package:rmt_walnut/Models/Response/OrderStats.dart';
import 'package:rmt_walnut/Models/Response/OtpVerifyResponse.dart';
import 'package:rmt_walnut/Models/Response/UserLoginResponse.dart';
import 'package:rmt_walnut/Utils/MyWidgets.dart';
import 'package:rmt_walnut/services/ApiRepo.dart';
import 'package:dio/dio.dart' show Dio, DioError;
import 'package:rmt_walnut/services/RestClient.dart';

import '../Models/Response/GetOrderbyIdResponse.dart';
import '../Models/Response/StoreUpdateresponse.dart';
import '../main.dart';

class ApiProvider extends APiRepo{


  Dio dio = Dio();

  BuildContext context;

  ApiProvider(this.context);
  @override
  Future<UserLoginResponse> userLogin(UserLoginRequest userLoginRequest) async {


    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.userLogin(userLoginRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");

          MyWidgets.showSnakbarMsg(context, "Un Expected Error");

          throw res;

        default:
          break;


      }
    });



  }

  @override
  Future<OtpVerifyResponse> userVerifyOtp(OtpVerifyRequest otpVerifyRequest) {
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.userVerifyOtp(otpVerifyRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");


          throw res;

        default:
          break;


      }
    });



    // TODO: implement userVerifyOtp

  }

  @override
  Future<OrderPostResponse> postOrder( OrderPostRequest orderPostRequest) {
    // TODO: implement postOrder
    // TODO: implement postOrder
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.postOrder(orderPostRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");


          throw res;

        default:
          break;


      }
    });


  }

  @override
  Future<GetOrdersResponse> getOrder(String type) {
    // TODO: implement getOrder
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.getOrder(OrderstatsRequest(stage: type)).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");


          throw res;

        default:
          break;


      }
    });

  }

  @override
  Future<OrderResponseById> getOrdersbyId(String id) {
    // TODO: implement getOrdersbyId
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.getOrdersbyId(id).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.

      logger.e(obj);
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");


          throw res;

        default:
          break;


      }
    });
  }

  @override
  Future<OrderResponseById> postDesign(DesignUpdateRequest designUpdateRequest) {
    // TODO: implement postDesign
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);

    return  restCLient.postDesign(designUpdateRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.

      logger.    e(obj);
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;

          logger.e("Got error : ${res?.statusCode} -> ${res?.statusMessage}");


          throw res!;

        default:
          break;


      }
    });
  }

  @override
  Future<StoreUpdateResponse> postStore(StoreUpdateRequest storeUpdateRequest) {
    // TODO: implement postStore
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);
    return  restCLient.postStore(storeUpdateRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      logger.e(obj);
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");
          throw res;
        default:
          break;
      }
    });
  }

  @override
  Future<String> postProdcution(ProductionUpdateRequest productionUpdateRequest) {
    // TODO: implement postProdcution
    // TODO: implement postStore
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);
    return  restCLient.postProdcution(productionUpdateRequest).then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      logger.e(obj);
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");
          throw res;
        default:
          break;
      }
    });
  }

  @override
  Future<Orderstats> getStats() {
    // TODO: implement getStats
    dio.interceptors.add(PrettyDioLogger());
// customization
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
    final restCLient = RestClient(dio);
    return  restCLient.getStats().then((value)  {
      return  value;
    }).catchError((Object obj) {
      // non-200 error goes here.
      logger.e(obj);
      switch (obj.runtimeType) {
        case DioError:
        // Here's the sample to get the failed response error code and message
          final res = (obj as DioError).response;
          logger.e("Got error : ${res?.statusCode} -> ${res!.statusMessage}");
          throw res;
        default:
          break;
      }
    });
  }
  }


