


import 'package:retrofit/retrofit.dart';

import '../Models/Request/DesignUpdateRequest.dart';
import '../Models/Request/OrderPost.dart';
import '../Models/Request/OtpRequest.dart';
import '../Models/Request/ProductionRequestModel.dart';
import '../Models/Request/StoreUpdateRequest.dart';
import '../Models/Request/UserLoginRequest.dart';
import '../Models/Response/GetOrderRespsonse.dart';
import '../Models/Response/GetOrderbyIdResponse.dart';
import '../Models/Response/OrderPostResponse.dart';
import '../Models/Response/OrderStats.dart';
import '../Models/Response/OtpVerifyResponse.dart';
import '../Models/Response/StoreUpdateresponse.dart';
import '../Models/Response/UserLoginResponse.dart';

abstract class APiRepo{


  Future<UserLoginResponse> userLogin(@Body() UserLoginRequest userLoginRequest);

  Future<OtpVerifyResponse> userVerifyOtp(@Body() OtpVerifyRequest otpVerifyRequest);

  Future<OrderPostResponse> postOrder(@Body() OrderPostRequest orderPostRequest);

  Future<GetOrdersResponse> getOrder( String type);

  Future<OrderResponseById> getOrdersbyId( @Path() String id);

  Future<OrderResponseById> postDesign(@Body() DesignUpdateRequest designUpdateRequest);


  Future<StoreUpdateResponse> postStore(@Body() StoreUpdateRequest storeUpdateRequest);

  Future<String> postProdcution(@Body() ProductionUpdateRequest productionUpdateRequest);


  Future<Orderstats> getStats();

}