import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:rmt_walnut/Models/Response/UserJwtResponse.dart';
import 'package:rmt_walnut/Provider/UserProvider.dart';
import 'package:rmt_walnut/UI/HomePage.dart';
import 'package:rmt_walnut/Models/Request/OtpRequest.dart';
import 'package:rmt_walnut/Models/Request/UserLoginRequest.dart';
import 'package:rmt_walnut/Models/Response/UserLoginResponse.dart';
import 'package:rmt_walnut/services/ApiProvider.dart';

import 'Utils/MyWidgets.dart';


String final_box = "Walnut_box";
Logger logger = Logger();
Future<void> main() async {
  await Hive.initFlutter();

  await Hive.openBox(final_box);


  runApp(  MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => UserProvider()),
    ],
    child: const MyApp(),
  ));

}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',

      initialRoute: '/',
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.
        '/': (context) => const MyHomePage(title: 'Flutter Demo Home Page'),
        // When navigating to the "/second" route, build the SecondScreen widget.
        '/home_page': (context) => const HomePage(),
      },


      theme: ThemeData(

        backgroundColor: const Color.fromRGBO(55, 126, 192, 100),
        primaryColor:const Color.fromRGBO(55, 126, 192, 100) ,

        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),

    //  home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  bool optSent = false;

  bool loading = false;
  bool otpverify = false;
late  String verifyotp;
 final _formKey = GlobalKey<FormState>();  TextEditingController  mobileController = TextEditingController();
  TextEditingController  textEditingController = TextEditingController();
  StreamController<ErrorAnimationType> errorController = StreamController<ErrorAnimationType>();

   String? otp;


  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(

      backgroundColor: const Color(0xFFEFEBEB),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(),
         //   Lottie.network("https://assets4.lottiefiles.com/packages/lf20_mdyrjbcc.json"),
            Image.asset("assets/walnutlogo 1.png"),

            IndexedStack(index: !optSent?0:1,children: [

              Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Container(padding: const EdgeInsets.all(18.0),height: size.height*0.3,
                    color: const Color(0xFFEFEBEB),child: Column(children: [


                      TextFormField(


                        controller: mobileController,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType:TextInputType.number,
                        maxLength: 10,
                        validator: (value){


                          if(value!.length <10){

                            return "Mobile Number Should be 10 digits";

                          }else{


                            return null;
                          }


                        },
                        inputFormatters: [

                          FilteringTextInputFormatter.digitsOnly

                        ],
                        decoration: const InputDecoration(filled: true,fillColor: Colors.white70,hintText: "+91 9876543210",prefixIcon: Icon(Icons.phone)),),

                 !loading ?     ElevatedButton(style: ElevatedButton.styleFrom( backgroundColor: Colors.indigo),onPressed: (){


                   setState(() {
                     loading = !loading;
                   });

                        if(_formKey.currentState!.validate()) {
                          ApiProvider(context)
                              .userLogin(
                              UserLoginRequest(phone: mobileController.text))
                              .then((value) {
                            otp = value.otp!;

                            optSent = !optSent;
                            logger.d(value);

                            setState(() {
                              loading = !loading;
                            });
                          }).catchError((Object obj) {

                            setState(() {
                              loading = !loading;
                            });
                            Map<String, dynamic> e = jsonDecode(obj.toString());

                            logger.d(e["staus"]);

                            MyWidgets.showSnakbarMsg(context, e["message"]);
                          });
                        }

                      }, child: SizedBox(width: size.width,child: const Center(child:  Text("Get Started")))):Center(child: CircularProgressIndicator()),


                      Text("Terms and Conditions")


                    ],)),
              ),
              Container(padding: const EdgeInsets.all(18.0),height: 260,
                  color: const Color(0xFFEFEBEB),child: Column(mainAxisSize: MainAxisSize.max,children: [

                    Align(alignment: Alignment.topLeft,child: IconButton(onPressed: (){

                      optSent = !optSent;
                      textEditingController.text = "";
                      mobileController.text ="";
                      FocusScope.of(context).unfocus();
                      setState(() {

                      });

                    }, icon: Icon(Icons.arrow_back))),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("Otp Sent to Registerd Mobile Number  "+otp.toString(),style: TextStyle(fontWeight: FontWeight.w700)),
                    ),
                    Spacer(),
                    PinCodeTextField(

                      length: 6,
                      obscureText: false,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(5),
                        fieldHeight: 50,
                        fieldWidth: 40,
                        activeColor: Colors.transparent,
                        inactiveColor: Colors.indigo,
                        errorBorderColor: Colors.red,
                        selectedFillColor: Colors.transparent,
                        inactiveFillColor: Colors.white,
                        activeFillColor: Colors.white,
                      ),
                      animationDuration: Duration(milliseconds: 300),

                      enableActiveFill: true,
                      errorAnimationController: errorController,
                      controller: textEditingController,
                      onCompleted: (v) {
                        if(v==otp){


                          verifyotp = v;
                          VerifyOtp(v, mobileController.text.toString());

                        }else{


                          errorController.add(ErrorAnimationType.shake);
                        }
                        print("Completed");
                      },
                      onChanged: (value) {
                        print(value);
                        setState(() {

                        });
                      },
                      beforeTextPaste: (text) {
                        print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      }, appContext: context,
                    ),



                    ElevatedButton(style: ElevatedButton.styleFrom( backgroundColor: Colors.indigo),onPressed: (){




                      if(verifyotp == otp){
                        VerifyOtp(verifyotp, mobileController.text.toString());
                      }else{
                        errorController.add(ErrorAnimationType.shake);

                      }



                    }, child: SizedBox(width: size.width,child: Center(child:  Text("Verify Otp")))),


                    Text("Resend Otp")


                  ],)),

            ],)
          ],
        ),
      ),

    );
  }


  VerifyOtp(String Otp,String Mobile){


    ApiProvider(context).userVerifyOtp(OtpVerifyRequest(phone: mobileController.text.toString(), phoneOtp: Otp)).then((value) {

      var box1 = Hive.box(final_box);

      UserProvider userProvider =  Provider.of<UserProvider>(context, listen: false);
      Map<String,dynamic>   d = JwtDecoder.decode(value.acessToken);

      print(d['user']);

      userProvider.userJwtResponse = UserJwtResponse(id:d['user']["_id"],email: d['user']["email"],permisions: d['user']["permisions"] ,phone:d['user']["phone"] ,isAccountVerified:d['user']["isAccountVerified"] ,name:d['user']["name"] );


     box1.put("access_Token", value.acessToken).then((value) {


       Navigator.pushReplacementNamed(context, '/home_page');
      });


    }).catchError((Object obj) {

      setState(() {
        loading = !loading;
      });
      Map<String, dynamic> e = jsonDecode(obj.toString());

      logger.d(e["staus"]);

      MyWidgets.showSnakbarMsg(context, e["message"]);
    });


  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   // var a =    Hive.box(box).get("access_Token");
//    print(a);
getLogin();

  }



  getLogin() async {
    var box1 =Hive.box(final_box);
    var  a =  box1.get("access_Token");
    if (kDebugMode) {
      print(a);
    }
    if(a!=null){

      UserProvider userProvider =  Provider.of<UserProvider>(context, listen: false);
      Map<String,dynamic>   d = JwtDecoder.decode(a);

      print(d['user']);

      userProvider.userJwtResponse = UserJwtResponse(id:d['user']["_id"],email: d['user']["email"],permisions: d['user']["permisions"] ,phone:d['user']["phone"] ,isAccountVerified:d['user']["isAccountVerified"] ,name:d['user']["name"] );


      Future.delayed(const Duration(milliseconds: 100),(){
        Navigator.pushReplacementNamed(context, '/home_page');

      });





    }
  }
}
