// To parse this JSON data, do
//
//     final designUpdateRequest = designUpdateRequestFromJson(jsonString);

import 'dart:convert';

DesignUpdateRequest designUpdateRequestFromJson(String str) => DesignUpdateRequest.fromJson(json.decode(str));

String designUpdateRequestToJson(DesignUpdateRequest data) => json.encode(data.toJson());

class DesignUpdateRequest {
  DesignUpdateRequest({
    this.orderId,
    this.plates,
    this.dye,
    this.positives,
    this.platesDeliverDate,
    this.dyeDeliverDate,
  });

  String? orderId;
  String? plates;
  String? dye;
  String? positives;
  String? platesDeliverDate;
  String? dyeDeliverDate;

  factory DesignUpdateRequest.fromJson(Map<String, dynamic> json) => DesignUpdateRequest(
    orderId: json["order_id"] == null ? null : json["order_id"],
    plates: json["plates"] == null ? null : json["plates"],
    dye: json["dye"] == null ? null : json["dye"],
    positives: json["positives"] == null ? null : json["positives"],
    platesDeliverDate: json["plates_deliverDate"] == null ? null : json["plates_deliverDate"],
    dyeDeliverDate: json["dye_deliverDate"] == null ? null : json["dye_deliverDate"],
  );

  Map<String, dynamic> toJson() => {
    "order_id": orderId == null ? null : orderId,
    "plates": plates == null ? null : plates,
    "dye": dye == null ? null : dye,
    "positives": positives == null ? null : positives,
    "plates_deliverDate": platesDeliverDate == null ? null : platesDeliverDate,
    "dye_deliverDate": dyeDeliverDate == null ? null : dyeDeliverDate,
  };
}
