// To parse this JSON data, do
//
//     final productionUpdateRequest = productionUpdateRequestFromJson(jsonString);

import 'dart:convert';

ProductionUpdateRequest productionUpdateRequestFromJson(String str) => ProductionUpdateRequest.fromJson(json.decode(str));

String productionUpdateRequestToJson(ProductionUpdateRequest data) => json.encode(data.toJson());

class ProductionUpdateRequest {
  ProductionUpdateRequest({
    required    this.orderId,
  required  this.printing,
   required  this.varnish,
  required   this.lamination,
  required   this.punching,
  required   this.stripingDressing,
 required    this.foldingGuling,
  });
  String orderId;
  String printing;
  String varnish;
  String lamination;
  String punching;
  String stripingDressing;
  String foldingGuling;

  factory ProductionUpdateRequest.fromJson(Map<String, dynamic> json) => ProductionUpdateRequest(
    orderId: json["order_id"] == null ? null : json["order_id"],
    printing: json["printing"] == null ? null : json["printing"],
    varnish: json["varnish"] == null ? null : json["varnish"],
    lamination: json["lamination"] == null ? null : json["lamination"],
    punching: json["punching"] == null ? null : json["punching"],
    stripingDressing: json["striping_dressing"] == null ? null : json["striping_dressing"],
    foldingGuling: json["folding_guling"] == null ? null : json["folding_guling"],
  );

  Map<String, dynamic> toJson() => {
    "order_id": orderId == null ? null : orderId,
    "printing": printing == null ? null : printing,
    "varnish": varnish == null ? null : varnish,
    "lamination": lamination == null ? null : lamination,
    "punching": punching == null ? null : punching,
    "striping_dressing": stripingDressing == null ? null : stripingDressing,
    "folding_guling": foldingGuling == null ? null : foldingGuling,
  };
}
