// To parse this JSON data, do
//
//     final storeUpdateRequest = storeUpdateRequestFromJson(jsonString);

import 'dart:convert';

StoreUpdateRequest storeUpdateRequestFromJson(String str) => StoreUpdateRequest.fromJson(json.decode(str));

String storeUpdateRequestToJson(StoreUpdateRequest data) => json.encode(data.toJson());

class StoreUpdateRequest {
  StoreUpdateRequest({
 required    this.orderId,
  required this.inks,
  required  this.board,
   required this.varnish,
  required   this.lamination,
 required   this.boardDetailsType,
    this.boardDetailsDate,
    this.inksDetailsDate,
    this.varnishDetailsDate,
    this.laminationDetailsDate,
  required  this.inksDetailsType,
  required  this.varnishDetailsType,
  required  this.laminationDetailsType,
  });

  String orderId;
  bool inks;
  bool board;
  bool varnish;
  bool lamination;
   String boardDetailsType;
  String? boardDetailsDate;
  String? inksDetailsDate;
  String? varnishDetailsDate;
  String? laminationDetailsDate;
  String inksDetailsType;
  String varnishDetailsType;
  String laminationDetailsType;

  factory StoreUpdateRequest.fromJson(Map<String, dynamic> json) => StoreUpdateRequest(
    orderId: json["order_id"] == null ? null : json["order_id"],
    inks: json["inks"] == null ? null : json["inks"],
    board: json["board"] == null ? null : json["board"],
    varnish: json["varnish"] == null ? null : json["varnish"],
    lamination: json["lamination"] == null ? null : json["lamination"],
    boardDetailsType: json["board_details_type"] == null ? null : json["board_details_type"],
    boardDetailsDate: json["board_details_date"] == null ? null : json["board_details_date"],
    inksDetailsDate: json["inks_details_date"],
    varnishDetailsDate: json["varnish_details_date"],
    laminationDetailsDate: json["lamination_details_date"],
    inksDetailsType: json["inks_details_type"] == null ? null : json["inks_details_type"],
    varnishDetailsType: json["varnish_details_type"] == null ? null : json["varnish_details_type"],
    laminationDetailsType: json["lamination_details_type"] == null ? null : json["lamination_details_type"],
  );

  Map<String, dynamic> toJson() => {
    "order_id": orderId == null ? null : orderId,
    "inks": inks == null ? null : inks,
    "board": board == null ? null : board,
    "varnish": varnish == null ? null : varnish,
    "lamination": lamination == null ? null : lamination,
    "board_details_type": boardDetailsType == null ? null : boardDetailsType,
    "board_details_date": boardDetailsDate == null ? null : boardDetailsDate,
    "inks_details_date": inksDetailsDate,
    "varnish_details_date": varnishDetailsDate,
    "lamination_details_date": laminationDetailsDate,
    "inks_details_type": inksDetailsType == null ? null : inksDetailsType,
    "varnish_details_type": varnishDetailsType == null ? null : varnishDetailsType,
    "lamination_details_type": laminationDetailsType == null ? null : laminationDetailsType,
  };
}
