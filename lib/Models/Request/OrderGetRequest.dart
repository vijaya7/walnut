// To parse this JSON data, do
//
//     final orderstatsRequest = orderstatsRequestFromJson(jsonString);

import 'dart:convert';

OrderstatsRequest orderstatsRequestFromJson(String str) => OrderstatsRequest.fromJson(json.decode(str));

String orderstatsRequestToJson(OrderstatsRequest data) => json.encode(data.toJson());

class OrderstatsRequest {
  OrderstatsRequest({
  required  this.stage,
  });

  String stage;

  factory OrderstatsRequest.fromJson(Map<String, dynamic> json) => OrderstatsRequest(
    stage: json["stage"] == null ? null : json["stage"],
  );

  Map<String, dynamic> toJson() => {
    "stage": stage == null ? null : stage,
  };
}
