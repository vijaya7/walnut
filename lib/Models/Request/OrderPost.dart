// To parse this JSON data, do
//
//     final orderPostRequest = orderPostRequestFromJson(jsonString);

import 'dart:convert';

OrderPostRequest orderPostRequestFromJson(String str) => OrderPostRequest.fromJson(json.decode(str));

String orderPostRequestToJson(OrderPostRequest data) => json.encode(data.toJson());

class OrderPostRequest {
  OrderPostRequest({
    this.customerName,
    this.customerNumber,
    this.jobName,
    this.poNumber,
    this.specQuantity,
    this.specBoardType,
    this.specInks,
    this.specVarnish,
    this.specLamination,
  });

  String? customerName;
  String? customerNumber;
  String? jobName;
  String? poNumber;
  String? specQuantity;
  String? specBoardType;
  String? specInks;
  String? specVarnish;
  String? specLamination;

  factory OrderPostRequest.fromJson(Map<String, dynamic> json) => OrderPostRequest(
    customerName: json["customer_name"],
    customerNumber: json["customer_number"],
    jobName: json["job_name"],
    poNumber: json["po_number"],
    specQuantity: json["spec_quantity"],
    specBoardType: json["spec_board_type"],
    specInks: json["spec_inks"],
    specVarnish: json["spec_varnish"],
    specLamination: json["spec_lamination"],
  );

  Map<String, dynamic> toJson() => {
    "customer_name": customerName,
    "customer_number": customerNumber,
    "job_name": jobName,
    "po_number": poNumber,
    "spec_quantity": specQuantity,
    "spec_board_type": specBoardType,
    "spec_inks": specInks,
    "spec_varnish": specVarnish,
    "spec_lamination": specLamination,
  };
}
