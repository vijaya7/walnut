// To parse this JSON data, do
//
//     final otpVerifyRequest = otpVerifyRequestFromJson(jsonString);

import 'dart:convert';

OtpVerifyRequest otpVerifyRequestFromJson(String str) => OtpVerifyRequest.fromJson(json.decode(str));

String otpVerifyRequestToJson(OtpVerifyRequest data) => json.encode(data.toJson());

class OtpVerifyRequest {
  OtpVerifyRequest({
   required  this.phone,
   required this.phoneOtp,
  });

  String phone;
  String phoneOtp;

  factory OtpVerifyRequest.fromJson(Map<String, dynamic> json) => OtpVerifyRequest(
    phone: json["phone"],
    phoneOtp: json["phone_otp"],
  );

  Map<String, dynamic> toJson() => {
    "phone": phone,
    "phone_otp": phoneOtp,
  };
}
