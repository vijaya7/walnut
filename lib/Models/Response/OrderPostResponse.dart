// To parse this JSON data, do
//
//     final orderPostResponse = orderPostResponseFromJson(jsonString);

import 'dart:convert';

OrderPostResponse orderPostResponseFromJson(String str) => OrderPostResponse.fromJson(json.decode(str));

String orderPostResponseToJson(OrderPostResponse data) => json.encode(data.toJson());

class OrderPostResponse {
  OrderPostResponse({
    this.result,
  });

  Result? result;

  factory OrderPostResponse.fromJson(Map<String, dynamic> json) => OrderPostResponse(
    result: Result.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "result": result!.toJson(),
  };
}

class Result {
  Result({
    this.customerName,
    this.jobName,
    this.customerNumber,
    this.poNumber,
    this.poRegisterDate,
    this.poDispatchDate,
    this.specQuantity,
    this.stage,
    this.specBoardType,
    this.specInks,
    this.specVarnish,
    this.specLamination,
    this.id,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String? customerName;
  String? jobName;
  int? customerNumber;
  String? poNumber;
  DateTime? poRegisterDate;
  DateTime? poDispatchDate;
  int? specQuantity;
  String? stage;
  String? specBoardType;
  String? specInks;
  String? specVarnish;
  String? specLamination;
  String? id;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    customerName: json["customer_name"],
    jobName: json["job_name"],
    customerNumber: json["customer_number"],
    poNumber: json["po_number"],
    poRegisterDate: DateTime.parse(json["po_register_date"]),
    poDispatchDate: DateTime.parse(json["po_dispatch_date"]),
    specQuantity: json["spec_quantity"],
    stage: json["stage"],
    specBoardType: json["spec_board_type"],
    specInks: json["spec_inks"],
    specVarnish: json["spec_varnish"],
    specLamination: json["spec_lamination"],
    id: json["_id"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "customer_name": customerName,
    "job_name": jobName,
    "customer_number": customerNumber,
    "po_number": poNumber,
    "po_register_date": poRegisterDate!.toIso8601String(),
    "po_dispatch_date": poDispatchDate!.toIso8601String(),
    "spec_quantity": specQuantity,
    "stage": stage,
    "spec_board_type": specBoardType,
    "spec_inks": specInks,
    "spec_varnish": specVarnish,
    "spec_lamination": specLamination,
    "_id": id,
    "createdAt": createdAt!.toIso8601String(),
    "updatedAt": updatedAt!.toIso8601String(),
    "__v": v,
  };
}
