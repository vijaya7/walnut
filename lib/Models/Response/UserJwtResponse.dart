// To parse this JSON data, do
//
//     final userJwtResponse = userJwtResponseFromJson(jsonString);

import 'dart:convert';

UserJwtResponse userJwtResponseFromJson(String str) => UserJwtResponse.fromJson(json.decode(str));

String userJwtResponseToJson(UserJwtResponse data) => json.encode(data.toJson());

class UserJwtResponse {
  UserJwtResponse({
    this.id,
    this.name,
    this.phone,
    this.permisions,
    this.email,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.phoneOtp,
    this.isAccountVerified,
    this.iat,
    this.exp,
  });

  String? id;
  String? name;
  int? phone;
  List<dynamic>? permisions;
  String? email;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;
  int? phoneOtp;
  bool? isAccountVerified;
  String? iat;
  String? exp;

  factory UserJwtResponse.fromJson(Map<String, dynamic> json) => UserJwtResponse(
    id: json["_id"] == null ? null : json["_id"],
    name: json["name"] == null ? null : json["name"],
    phone: json["phone"] == null ? null : json["phone"],
    permisions: json["permisions"] == null ? null : List<String>.from(json["permisions"].map((x) => x)),
    email: json["email"] == null ? null : json["email"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    v: json["__v"] == null ? null : json["__v"],
    phoneOtp: json["phone_otp"] == null ? null : json["phone_otp"],
    isAccountVerified: json["isAccountVerified"] == null ? null : json["isAccountVerified"],
    iat: json["iat"] == null ? null : json["iat"],
    exp: json["exp"] == null ? null : json["exp"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id == null ? null : id,
    "name": name == null ? null : name,
    "phone": phone == null ? null : phone,
    "permisions": permisions == null ? null : List<dynamic>.from(permisions!.map((x) => x)),
    "email": email == null ? null : email,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
    "__v": v == null ? null : v,
    "phone_otp": phoneOtp == null ? null : phoneOtp,
    "isAccountVerified": isAccountVerified == null ? null : isAccountVerified,
    "iat": iat == null ? null : iat,
    "exp": exp == null ? null : exp,
  };
}
