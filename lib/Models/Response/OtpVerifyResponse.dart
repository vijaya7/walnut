// To parse this JSON data, do
//
//     final otpVerifyResponse = otpVerifyResponseFromJson(jsonString);

import 'dart:convert';

OtpVerifyResponse otpVerifyResponseFromJson(String str) => OtpVerifyResponse.fromJson(json.decode(str));

String otpVerifyResponseToJson(OtpVerifyResponse data) => json.encode(data.toJson());

class OtpVerifyResponse {
  OtpVerifyResponse({
  required  this.status,
  required  this.message,
   required this.acessToken,
  });

  bool status;
  String message;
  String acessToken;

  factory OtpVerifyResponse.fromJson(Map<String, dynamic> json) => OtpVerifyResponse(
    status: json["status"],
    message: json["message"],
    acessToken: json["acessToken"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "acessToken": acessToken,
  };
}
