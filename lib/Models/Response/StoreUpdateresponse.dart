// To parse this JSON data, do
//
//     final storeUpdateResponse = storeUpdateResponseFromJson(jsonString);

import 'dart:convert';

StoreUpdateResponse storeUpdateResponseFromJson(String str) => StoreUpdateResponse.fromJson(json.decode(str));

String storeUpdateResponseToJson(StoreUpdateResponse data) => json.encode(data.toJson());

class StoreUpdateResponse {
  StoreUpdateResponse({
    this.id,
    this.orderDetails,
    this.orderStatus,
    this.createdAt,
    this.updatedAt,
    this.v,
    this.desing,
    this.production,
    this.store,
    this.dispatch,
  });

  String? id;
  String? orderDetails;
  String? orderStatus;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;
  Desing? desing;
  Production? production;
  Store? store;
  Dispatch? dispatch;

  factory StoreUpdateResponse.fromJson(Map<String, dynamic> json) => StoreUpdateResponse(
    id: json["_id"] == null ? null : json["_id"],
    orderDetails: json["order_details"] == null ? null : json["order_details"],
    orderStatus: json["order_status"] == null ? null : json["order_status"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    v: json["__v"] == null ? null : json["__v"],
    desing: json["desing"] == null ? null : Desing.fromJson(json["desing"]),
    production: json["production"] == null ? null : Production.fromJson(json["production"]),
    store: json["store"] == null ? null : Store.fromJson(json["store"]),
    dispatch: json["dispatch"] == null ? null : Dispatch.fromJson(json["dispatch"]),
  );

  Map<String, dynamic> toJson() => {
    "_id": id == null ? null : id,
    "order_details": orderDetails == null ? null : orderDetails,
    "order_status": orderStatus == null ? null : orderStatus,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
    "__v": v == null ? null : v,
    "desing": desing == null ? null : desing!.toJson(),
    "production": production == null ? null : production!.toJson(),
    "store": store == null ? null : store!.toJson(),
    "dispatch": dispatch == null ? null : dispatch!.toJson(),
  };
}

class Desing {
  Desing({
    this.desing,
    this.plates,
    this.dye,
    this.positives,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  bool? desing;
  String? plates;
  String? dye;
  String? positives;
  String? id;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Desing.fromJson(Map<String, dynamic> json) => Desing(
    desing: json["desing"] == null ? null : json["desing"],
    plates: json["plates"] == null ? null : json["plates"],
    dye: json["dye"] == null ? null : json["dye"],
    positives: json["positives"] == null ? null : json["positives"],
    id: json["_id"] == null ? null : json["_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "desing": desing == null ? null : desing,
    "plates": plates == null ? null : plates,
    "dye": dye == null ? null : dye,
    "positives": positives == null ? null : positives,
    "_id": id == null ? null : id,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}

class Dispatch {
  Dispatch({
    this.uploadImages,
    this.uploadInvoices,
    this.id,
    this.updatedAt,
    this.createdAt,
  });

  List<String>? uploadImages;
  List<String>? uploadInvoices;
  String? id;
  DateTime? updatedAt;
  DateTime? createdAt;

  factory Dispatch.fromJson(Map<String, dynamic> json) => Dispatch(
    uploadImages: json["upload_images"] == null ? null : List<String>.from(json["upload_images"].map((x) => x)),
    uploadInvoices: json["upload_invoices"] == null ? null : List<String>.from(json["upload_invoices"].map((x) => x)),
    id: json["_id"] == null ? null : json["_id"],
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
  );

  Map<String, dynamic> toJson() => {
    "upload_images": uploadImages == null ? null : List<dynamic>.from(uploadImages!.map((x) => x)),
    "upload_invoices": uploadInvoices == null ? null : List<dynamic>.from(uploadInvoices!.map((x) => x)),
    "_id": id == null ? null : id,
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
  };
}

class Production {
  Production({
    this.printing,
    this.varnish,
    this.lamination,
    this.punching,
    this.stripingDressing,
    this.foldingGuling,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  String? printing;
  String? varnish;
  String? lamination;
  String? punching;
  String? stripingDressing;
  String? foldingGuling;
  String? id;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Production.fromJson(Map<String, dynamic> json) => Production(
    printing: json["printing"] == null ? null : json["printing"],
    varnish: json["varnish"] == null ? null : json["varnish"],
    lamination: json["lamination"] == null ? null : json["lamination"],
    punching: json["punching"] == null ? null : json["punching"],
    stripingDressing: json["striping_dressing"] == null ? null : json["striping_dressing"],
    foldingGuling: json["folding_guling"] == null ? null : json["folding_guling"],
    id: json["_id"] == null ? null : json["_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "printing": printing == null ? null : printing,
    "varnish": varnish == null ? null : varnish,
    "lamination": lamination == null ? null : lamination,
    "punching": punching == null ? null : punching,
    "striping_dressing": stripingDressing == null ? null : stripingDressing,
    "folding_guling": foldingGuling == null ? null : foldingGuling,
    "_id": id == null ? null : id,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}

class Store {
  Store({
    this.board,
    this.boardDetails,
    this.inks,
    this.inkDetails,
    this.varnish,
    this.varnishDetails,
    this.lamination,
    this.laminationDetails,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  bool? board;
  Details? boardDetails;
  bool? inks;
  Details? inkDetails;
  bool? varnish;
  Details? varnishDetails;
  bool? lamination;
  Details? laminationDetails;
  String? id;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
    board: json["board"] == null ? null : json["board"],
    boardDetails: json["board_details"] == null ? null : Details.fromJson(json["board_details"]),
    inks: json["inks"] == null ? null : json["inks"],
    inkDetails: json["ink_details"] == null ? null : Details.fromJson(json["ink_details"]),
    varnish: json["varnish"] == null ? null : json["varnish"],
    varnishDetails: json["varnish_details"] == null ? null : Details.fromJson(json["varnish_details"]),
    lamination: json["lamination"] == null ? null : json["lamination"],
    laminationDetails: json["lamination_details"] == null ? null : Details.fromJson(json["lamination_details"]),
    id: json["_id"] == null ? null : json["_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "board": board == null ? null : board,
    "board_details": boardDetails == null ? null : boardDetails!.toJson(),
    "inks": inks == null ? null : inks,
    "ink_details": inkDetails == null ? null : inkDetails!.toJson(),
    "varnish": varnish == null ? null : varnish,
    "varnish_details": varnishDetails == null ? null : varnishDetails!.toJson(),
    "lamination": lamination == null ? null : lamination,
    "lamination_details": laminationDetails == null ? null : laminationDetails!.toJson(),
    "_id": id == null ? null : id,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}

class Details {
  Details({
    this.details,
    this.deliveryDate,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  String? details;
  DateTime? deliveryDate;
  String? id;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory Details.fromJson(Map<String, dynamic> json) => Details(
    details: json["details"] == null ? null : json["details"],
    deliveryDate: json["delivery_date"] == null ? null : DateTime.parse(json["delivery_date"]),
    id: json["_id"] == null ? null : json["_id"],
    createdAt: json["createdAt"] == null ? null : DateTime.parse(json["createdAt"]),
    updatedAt: json["updatedAt"] == null ? null : DateTime.parse(json["updatedAt"]),
  );

  Map<String, dynamic> toJson() => {
    "details": details == null ? null : details,
    "delivery_date": deliveryDate == null ? null : deliveryDate!.toIso8601String(),
    "_id": id == null ? null : id,
    "createdAt": createdAt == null ? null : createdAt!.toIso8601String(),
    "updatedAt": updatedAt == null ? null : updatedAt!.toIso8601String(),
  };
}
