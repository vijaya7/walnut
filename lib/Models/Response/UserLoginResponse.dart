// To parse this JSON data, do
//
//     final userLoginResponse = userLoginResponseFromJson(jsonString);

import 'dart:convert';

UserLoginResponse userLoginResponseFromJson(String str) => UserLoginResponse.fromJson(json.decode(str));

String userLoginResponseToJson(UserLoginResponse data) => json.encode(data.toJson());

class UserLoginResponse {
  UserLoginResponse({
    this.staus,
    this.message,
    this.otp,
  });

  bool? staus;
  String? message;
  String? otp;

  factory UserLoginResponse.fromJson(Map<String, dynamic> json) => UserLoginResponse(
    staus: json["staus"],
    message: json["message"],
    otp: json["otp"],
  );

  Map<String, dynamic> toJson() => {
    "staus": staus,
    "message": message,
    "otp": otp,
  };
}
