// To parse this JSON data, do
//
//     final orderstats = orderstatsFromJson(jsonString);

import 'dart:convert';

Orderstats orderstatsFromJson(String str) => Orderstats.fromJson(json.decode(str));

String orderstatsToJson(Orderstats data) => json.encode(data.toJson());

class Orderstats {
  Orderstats({
   required this.result,
  });

  List<Result>? result;

  factory Orderstats.fromJson(Map<String, dynamic> json) => Orderstats(
    result: json["result"] == null ? null : List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "result": result == null ? null : List<dynamic>.from(result!.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.staus,
    this.count,
  });

  String? staus;
  int? count;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    staus: json["status"] == null ? null : json["status"],
    count: json["count"] == null ? null : json["count"],
  );

  Map<String, dynamic> toJson() => {
    "status": staus == null ? null : staus,
    "count": count == null ? null : count,
  };
}
