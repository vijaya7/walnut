// To parse this JSON data, do
//
//     final getOrdersResponse = getOrdersResponseFromJson(jsonString);

import 'dart:convert';

GetOrdersResponse getOrdersResponseFromJson(String str) => GetOrdersResponse.fromJson(json.decode(str));

String getOrdersResponseToJson(GetOrdersResponse data) => json.encode(data.toJson());

class GetOrdersResponse {
  GetOrdersResponse({
    this.result,
  });

  List<Result>? result;

  factory GetOrdersResponse.fromJson(Map<String, dynamic> json) => GetOrdersResponse(
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "result": List<dynamic>.from(result!.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.id,
    this.customerName,
    this.jobName,
    this.customerNumber,
    this.poNumber,
    this.poRegisterDate,
    this.poDispatchDate,
    this.specQuantity,
    this.stage,
    this.specBoardType,
    this.specInks,
    this.specVarnish,
    this.specLamination,
    this.createdAt,
    this.updatedAt,
    this.v,
  });

  String? id;
  String? customerName;
  String? jobName;
  int? customerNumber;
  String? poNumber;
  DateTime? poRegisterDate;
  DateTime? poDispatchDate;
  int? specQuantity;
  String? stage;
  String? specBoardType;
  String? specInks;
  String? specVarnish;
  String? specLamination;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["_id"],
    customerName: json["customer_name"],
    jobName: json["job_name"],
    customerNumber: json["customer_number"] == null ? null : json["customer_number"],
    poNumber: json["po_number"],
    poRegisterDate: DateTime.parse(json["po_register_date"]),
    poDispatchDate: DateTime.parse(json["po_dispatch_date"]),
    specQuantity: json["spec_quantity"],
    stage: json["stage"],
    specBoardType: json["spec_board_type"],
    specInks: json["spec_inks"],
    specVarnish: json["spec_varnish"],
    specLamination: json["spec_lamination"],
    createdAt: DateTime.parse(json["createdAt"]),
    updatedAt: DateTime.parse(json["updatedAt"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "customer_name": customerName,
    "job_name": jobName,
    "customer_number": customerNumber == null ? null : customerNumber,
    "po_number": poNumber,
    "po_register_date": poRegisterDate!.toIso8601String(),
    "po_dispatch_date": poDispatchDate!.toIso8601String(),
    "spec_quantity": specQuantity,
    "stage": stage,
    "spec_board_type": specBoardType,
    "spec_inks": specInks,
    "spec_varnish": specVarnish,
    "spec_lamination": specLamination,
    "createdAt": createdAt!.toIso8601String(),
    "updatedAt": updatedAt!.toIso8601String(),
    "__v": v,
  };
}
