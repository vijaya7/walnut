

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyWidgets {



  static showSnakbarMsg(BuildContext context, String value) {
    Future.delayed(Duration.zero, () {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(backgroundColor: Colors.blue, content: Text(value)));
    });
  }



}


