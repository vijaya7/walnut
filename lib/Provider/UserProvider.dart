


import 'package:flutter/cupertino.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:rmt_walnut/Models/Response/UserJwtResponse.dart';

class UserProvider extends ChangeNotifier{


 late UserJwtResponse userJwtResponse;

  setUserData(String accessToken){

    Map<String,dynamic>   d = JwtDecoder.decode(accessToken);

    print(d['user']);

    userJwtResponse = UserJwtResponse(id:d['user']["_id"],email: d['user']["email"],permisions: d['user']["permisions"] ,phone:d['user']["phone"] ,isAccountVerified:d['user']["isAccountVerified"] ,name:d['user']["name"] );

    notifyListeners();
  }






}